const Web3 = require("web3");
const TX = require('ethereumjs-tx')
var Buffer = require('buffer/').Buffer

const web3 = new Web3(new Web3.providers.HttpProvider('https://ropsten.infura.io/Vr1GWcLG0XzcdrZHWMPu'));

const pribuf = Buffer.from("81485D4122FEC015C6ED5F7C673A44DB4FFC69F7EC2F0B6995DD53C3D568686C", "hex")
var publicKey = (web3.eth.accounts.privateKeyToAccount('81485D4122FEC015C6ED5F7C673A44DB4FFC69F7EC2F0B6995DD53C3D568686C')).address;
var pubkey = "0x2e018dA742cD53E0AaA39a3DfbCc7e00898C643e";

function transactionfunction(methodtype, contracttype, methodname, arguments, msgvalue) {
    let contractaddress;
    let contract;
    let Abi;
    let contractFunction;
    if (contracttype == "TokenRegistry") {
        console.log("TokenRegistry");
        contractaddress = "0x0AbEafAE9333F7dC64959914f67765eDEe5FD05F";
        Abi = [{ "constant": true, "inputs": [], "name": "expiryLimit", "outputs": [{ "name": "", "type": "uint256" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [{ "name": "_symbol", "type": "string" }], "name": "getDetails", "outputs": [{ "name": "", "type": "address" }, { "name": "", "type": "uint256" }, { "name": "", "type": "string" }, { "name": "", "type": "bytes32" }, { "name": "", "type": "bool" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": false, "inputs": [{ "name": "_newExpiry", "type": "uint256" }], "name": "changeExpiryLimit", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [{ "name": "_stRegistry", "type": "address" }], "name": "setTokenRegistry", "outputs": [{ "name": "", "type": "bool" }], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": true, "inputs": [], "name": "owner", "outputs": [{ "name": "", "type": "address" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": false, "inputs": [{ "name": "_symbol", "type": "string" }, { "name": "_owner", "type": "address" }, { "name": "_tokenName", "type": "string" }], "name": "checkValidity", "outputs": [{ "name": "", "type": "bool" }], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": true, "inputs": [], "name": "strAddress", "outputs": [{ "name": "", "type": "address" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": false, "inputs": [{ "name": "_owner", "type": "address" }, { "name": "_symbol", "type": "string" }, { "name": "_tokenName", "type": "string" }, { "name": "_swarmHash", "type": "bytes32" }], "name": "registerTicker", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [{ "name": "newOwner", "type": "address" }], "name": "transferOwnership", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "inputs": [], "payable": false, "stateMutability": "nonpayable", "type": "constructor" }, { "anonymous": false, "inputs": [{ "indexed": true, "name": "_owner", "type": "address" }, { "indexed": false, "name": "_symbol", "type": "string" }, { "indexed": false, "name": "_name", "type": "string" }, { "indexed": false, "name": "_swarmHash", "type": "bytes32" }, { "indexed": false, "name": "_timestamp", "type": "uint256" }], "name": "LogRegisterTicker", "type": "event" }, { "anonymous": false, "inputs": [{ "indexed": false, "name": "_oldExpiry", "type": "uint256" }, { "indexed": false, "name": "_newExpiry", "type": "uint256" }], "name": "LogChangeExpiryLimit", "type": "event" }, { "anonymous": false, "inputs": [{ "indexed": true, "name": "previousOwner", "type": "address" }, { "indexed": true, "name": "newOwner", "type": "address" }], "name": "OwnershipTransferred", "type": "event" }]
        contract = new web3.eth.Contract(Abi, contractaddress)

    }
    else if (contracttype == "SecurityTokenRegistry") {
        console.log("SecurityTokenRegistry")
        contractaddress = "0x8e736d9cAeB9EB9B7A766C196FbB01eb53344e81";
        Abi = [{ "constant": false, "inputs": [{ "name": "_stVersionProxyAddress", "type": "address" }, { "name": "_version", "type": "bytes32" }], "name": "setProtocolVersion", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": true, "inputs": [], "name": "tickerRegistry", "outputs": [{ "name": "", "type": "address" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "protocolVersion", "outputs": [{ "name": "", "type": "bytes32" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [{ "name": "", "type": "bytes32" }], "name": "protocolVersionST", "outputs": [{ "name": "", "type": "address" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "owner", "outputs": [{ "name": "", "type": "address" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [{ "name": "_securityToken", "type": "address" }], "name": "getSecurityTokenData", "outputs": [{ "name": "", "type": "string" }, { "name": "", "type": "address" }, { "name": "", "type": "bytes32" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "moduleRegistry", "outputs": [{ "name": "", "type": "address" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": false, "inputs": [{ "name": "_name", "type": "string" }, { "name": "_symbol", "type": "string" }, { "name": "_decimals", "type": "uint8" }, { "name": "_tokenDetails", "type": "bytes32" }], "name": "generateSecurityToken", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [{ "name": "newOwner", "type": "address" }], "name": "transferOwnership", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": true, "inputs": [{ "name": "_symbol", "type": "string" }], "name": "getSecurityTokenAddress", "outputs": [{ "name": "", "type": "address" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "polyAddress", "outputs": [{ "name": "", "type": "address" }], "payable": false, "stateMutability": "view", "type": "function" }, { "inputs": [{ "name": "_polyAddress", "type": "address" }, { "name": "_moduleRegistry", "type": "address" }, { "name": "_tickerRegistry", "type": "address" }, { "name": "_stVersionProxy", "type": "address" }], "payable": false, "stateMutability": "nonpayable", "type": "constructor" }, { "anonymous": false, "inputs": [{ "indexed": false, "name": "_ticker", "type": "string" }, { "indexed": false, "name": "_securityTokenAddress", "type": "address" }, { "indexed": false, "name": "_owner", "type": "address" }], "name": "LogNewSecurityToken", "type": "event" }, { "anonymous": false, "inputs": [{ "indexed": true, "name": "previousOwner", "type": "address" }, { "indexed": true, "name": "newOwner", "type": "address" }], "name": "OwnershipTransferred", "type": "event" }]
        contract = new web3.eth.Contract(Abi, contractaddress)
    }
    else if (contracttype == "SecurityToken") {
        console.log("SecurityToken")
        contractaddress = "0x75A68967f449BCe0F70A589F95f9082ce295d6Ae";
        Abi = [{ "constant": true, "inputs": [], "name": "name", "outputs": [{ "name": "", "type": "string" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": false, "inputs": [{ "name": "_spender", "type": "address" }, { "name": "_value", "type": "uint256" }], "name": "approve", "outputs": [{ "name": "", "type": "bool" }], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": true, "inputs": [], "name": "totalSupply", "outputs": [{ "name": "", "type": "uint256" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": false, "inputs": [{ "name": "_moduleType", "type": "uint8" }, { "name": "_moduleIndex", "type": "uint8" }], "name": "removeModule", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [{ "name": "_from", "type": "address" }, { "name": "_to", "type": "address" }, { "name": "_value", "type": "uint256" }], "name": "transferFrom", "outputs": [{ "name": "success", "type": "bool" }], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": true, "inputs": [], "name": "decimals", "outputs": [{ "name": "", "type": "uint8" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "securityTokenVersion", "outputs": [{ "name": "", "type": "bytes32" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": false, "inputs": [{ "name": "_investor", "type": "address" }, { "name": "_amount", "type": "uint256" }], "name": "mint", "outputs": [{ "name": "success", "type": "bool" }], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": true, "inputs": [{ "name": "_moduleType", "type": "uint8" }, { "name": "_index", "type": "uint256" }], "name": "getModule", "outputs": [{ "name": "", "type": "bytes32" }, { "name": "", "type": "address" }, { "name": "", "type": "bool" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "MAX_MODULES", "outputs": [{ "name": "", "type": "uint8" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": false, "inputs": [{ "name": "_moduleType", "type": "uint8" }, { "name": "_moduleIndex", "type": "uint8" }, { "name": "_budget", "type": "uint256" }], "name": "changeModuleBudget", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": true, "inputs": [], "name": "STO_KEY", "outputs": [{ "name": "", "type": "uint8" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": false, "inputs": [{ "name": "_spender", "type": "address" }, { "name": "_subtractedValue", "type": "uint256" }], "name": "decreaseApproval", "outputs": [{ "name": "", "type": "bool" }], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": true, "inputs": [], "name": "polyToken", "outputs": [{ "name": "", "type": "address" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [{ "name": "_owner", "type": "address" }], "name": "balanceOf", "outputs": [{ "name": "balance", "type": "uint256" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [{ "name": "", "type": "uint8" }, { "name": "", "type": "uint256" }], "name": "modules", "outputs": [{ "name": "name", "type": "bytes32" }, { "name": "moduleAddress", "type": "address" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [{ "name": "", "type": "uint8" }], "name": "modulesLocked", "outputs": [{ "name": "", "type": "bool" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [{ "name": "_delegate", "type": "address" }, { "name": "_module", "type": "address" }, { "name": "_perm", "type": "bytes32" }], "name": "checkPermission", "outputs": [{ "name": "", "type": "bool" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "owner", "outputs": [{ "name": "", "type": "address" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "TRANSFERMANAGER_KEY", "outputs": [{ "name": "", "type": "uint8" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "symbol", "outputs": [{ "name": "", "type": "string" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [{ "name": "_from", "type": "address" }, { "name": "_to", "type": "address" }, { "name": "_amount", "type": "uint256" }], "name": "verifyTransfer", "outputs": [{ "name": "success", "type": "bool" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "PERMISSIONMANAGER_KEY", "outputs": [{ "name": "", "type": "uint8" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": false, "inputs": [{ "name": "_to", "type": "address" }, { "name": "_value", "type": "uint256" }], "name": "transfer", "outputs": [{ "name": "success", "type": "bool" }], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [{ "name": "_moduleFactory", "type": "address" }, { "name": "_data", "type": "bytes" }, { "name": "_maxCost", "type": "uint256" }, { "name": "_budget", "type": "uint256" }, { "name": "_locked", "type": "bool" }], "name": "addModule", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": true, "inputs": [], "name": "moduleRegistry", "outputs": [{ "name": "", "type": "address" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "tokenDetails", "outputs": [{ "name": "", "type": "bytes32" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": false, "inputs": [{ "name": "_spender", "type": "address" }, { "name": "_addedValue", "type": "uint256" }], "name": "increaseApproval", "outputs": [{ "name": "", "type": "bool" }], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": true, "inputs": [{ "name": "_owner", "type": "address" }, { "name": "_spender", "type": "address" }], "name": "allowance", "outputs": [{ "name": "", "type": "uint256" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": false, "inputs": [{ "name": "newOwner", "type": "address" }], "name": "transferOwnership", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [{ "name": "_amount", "type": "uint256" }], "name": "withdrawPoly", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": true, "inputs": [{ "name": "", "type": "address" }], "name": "investorStatus", "outputs": [{ "name": "_status", "type": "uint8" }], "payable": false, "stateMutability": "pure", "type": "function" }, { "inputs": [{ "name": "_name", "type": "string" }, { "name": "_symbol", "type": "string" }, { "name": "_decimals", "type": "uint8" }, { "name": "_tokenDetails", "type": "bytes32" }, { "name": "_securityTokenRegistry", "type": "address" }], "payable": false, "stateMutability": "nonpayable", "type": "constructor" }, { "anonymous": false, "inputs": [{ "indexed": true, "name": "_type", "type": "uint8" }, { "indexed": false, "name": "_name", "type": "bytes32" }, { "indexed": false, "name": "_moduleFactory", "type": "address" }, { "indexed": false, "name": "_module", "type": "address" }, { "indexed": false, "name": "_moduleCost", "type": "uint256" }, { "indexed": false, "name": "_budget", "type": "uint256" }, { "indexed": false, "name": "_timestamp", "type": "uint256" }], "name": "LogModuleAdded", "type": "event" }, { "anonymous": false, "inputs": [{ "indexed": true, "name": "_type", "type": "uint8" }, { "indexed": false, "name": "_module", "type": "address" }, { "indexed": false, "name": "_timestamp", "type": "uint256" }], "name": "LogModuleRemoved", "type": "event" }, { "anonymous": false, "inputs": [{ "indexed": true, "name": "_moduleType", "type": "uint8" }, { "indexed": false, "name": "_module", "type": "address" }, { "indexed": false, "name": "_budget", "type": "uint256" }], "name": "LogModuleBudgetChanged", "type": "event" }, { "anonymous": false, "inputs": [{ "indexed": true, "name": "to", "type": "address" }, { "indexed": false, "name": "amount", "type": "uint256" }], "name": "Mint", "type": "event" }, { "anonymous": false, "inputs": [{ "indexed": true, "name": "owner", "type": "address" }, { "indexed": true, "name": "spender", "type": "address" }, { "indexed": false, "name": "value", "type": "uint256" }], "name": "Approval", "type": "event" }, { "anonymous": false, "inputs": [{ "indexed": true, "name": "from", "type": "address" }, { "indexed": true, "name": "to", "type": "address" }, { "indexed": false, "name": "value", "type": "uint256" }], "name": "Transfer", "type": "event" }, { "anonymous": false, "inputs": [{ "indexed": true, "name": "previousOwner", "type": "address" }, { "indexed": true, "name": "newOwner", "type": "address" }], "name": "OwnershipTransferred", "type": "event" }]
        contract = new web3.eth.Contract(Abi, contractaddress)
    }
    else if (contracttype == "GeneralTransferManager") {
        console.log("GeneralTransferManager")
        contractaddress = "0x09A4f4deCcD275892197F3BeFD808Ea36480cEA2";
        Abi = [{ "constant": false, "inputs": [], "name": "getInitFunction", "outputs": [{ "name": "", "type": "bytes4" }], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": true, "inputs": [], "name": "WHITELIST", "outputs": [{ "name": "", "type": "bytes32" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "allowAllWhitelistTransfers", "outputs": [{ "name": "", "type": "bool" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": false, "inputs": [{ "name": "_allowAllTransfers", "type": "bool" }], "name": "changeAllowAllTransfers", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [{ "name": "_investor", "type": "address" }, { "name": "_fromTime", "type": "uint256" }, { "name": "_toTime", "type": "uint256" }], "name": "modifyWhitelist", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [{ "name": "_investors", "type": "address[]" }, { "name": "_fromTimes", "type": "uint256[]" }, { "name": "_toTimes", "type": "uint256[]" }], "name": "modifyWhitelistMulti", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [{ "name": "_allowAllWhitelistIssuances", "type": "bool" }], "name": "changeAllowAllWhitelistIssuances", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [{ "name": "_allowAllWhitelistTransfers", "type": "bool" }], "name": "changeAllowAllWhitelistTransfers", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": true, "inputs": [], "name": "FLAGS", "outputs": [{ "name": "", "type": "bytes32" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [{ "name": "_from", "type": "address" }, { "name": "_to", "type": "address" }, { "name": "", "type": "uint256" }], "name": "verifyTransfer", "outputs": [{ "name": "", "type": "bool" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [{ "name": "", "type": "address" }], "name": "whitelist", "outputs": [{ "name": "fromTime", "type": "uint256" }, { "name": "toTime", "type": "uint256" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "allowAllTransfers", "outputs": [{ "name": "", "type": "bool" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "issuanceAddress", "outputs": [{ "name": "", "type": "address" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "securityToken", "outputs": [{ "name": "", "type": "address" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "getPermissions", "outputs": [{ "name": "", "type": "bytes32[]" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "factory", "outputs": [{ "name": "", "type": "address" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": false, "inputs": [{ "name": "_issuanceAddress", "type": "address" }], "name": "changeIssuanceAddress", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": true, "inputs": [], "name": "allowAllWhitelistIssuances", "outputs": [{ "name": "", "type": "bool" }], "payable": false, "stateMutability": "view", "type": "function" }, { "inputs": [{ "name": "_securityToken", "type": "address" }], "payable": false, "stateMutability": "nonpayable", "type": "constructor" }, { "anonymous": false, "inputs": [{ "indexed": false, "name": "_issuanceAddress", "type": "address" }], "name": "LogChangeIssuanceAddress", "type": "event" }, { "anonymous": false, "inputs": [{ "indexed": false, "name": "_allowAllTransfers", "type": "bool" }], "name": "LogAllowAllTransfers", "type": "event" }, { "anonymous": false, "inputs": [{ "indexed": false, "name": "_allowAllWhitelistTransfers", "type": "bool" }], "name": "LogAllowAllWhitelistTransfers", "type": "event" }, { "anonymous": false, "inputs": [{ "indexed": false, "name": "_allowAllWhitelistIssuances", "type": "bool" }], "name": "LogAllowAllWhitelistIssuances", "type": "event" }, { "anonymous": false, "inputs": [{ "indexed": false, "name": "_investor", "type": "address" }, { "indexed": false, "name": "_dateAdded", "type": "uint256" }, { "indexed": false, "name": "_addedBy", "type": "address" }, { "indexed": false, "name": "_fromTime", "type": "uint256" }, { "indexed": false, "name": "_toTime", "type": "uint256" }], "name": "LogModifyWhitelist", "type": "event" }]
        contract = new web3.eth.Contract(Abi, contractaddress)
    }
    else {
        console.log("contract undefined");
        return;
    }
    if (methodtype == "payablefunction") {
        console.log("payable")
        if (arguments.length != 0) {
            contractFunction = contract.methods[methodname].apply(null, arguments);
        }
        else {
            contractFunction = contract.methods[methodname]();
        }

        web3.eth.getTransactionCount("0x2e018dA742cD53E0AaA39a3DfbCc7e00898C643e", function (err, non) {
            if (err) {
                console.error("ERROR", err);
            }
            else {
                var nonce = non.toString(16);
                let contractEncode = contractFunction.encodeABI();
                const Transparam = {
                    nonce: '0x' + nonce,
                    gasPrice: '0x4A817C800',
                    gasLimit: 4000000,
                    from: pubkey,
                    to: contractaddress,
                    value: msgvalue,
                    data: contractEncode,
                }
                const tx = new TX(Transparam);
                tx.sign(pribuf)
                web3.eth.sendSignedTransaction('0x' + (tx.serialize()).toString('hex'), function (err, transactionhash) {
                    if (!err) {
                        console.log(transactionhash);
                                    var hashstatusverify = setInterval(function () {
                                        web3.eth.getTransactionReceipt(transactionhash, function (err, result) {
                                            if (result !== null) {
                                                clearInterval(hashstatusverify);
                                                if (result.status == 0x1) {
                                                    console.log("Transaction success")
                                                }
                                                else {
                                                    console.log("Transaction failed")
                                                }
                                            }
                                        })

                                    }, 5000)                               
                                }
                         else {
                            console.error("ERROR", err);
                            }
                        })
                    }
                })
            }
            else if (methodtype == "constantfunction") {
                console.log("constant")
                if (arguments.length != 0) {
                    contractFunction = contract.methods[methodname].apply(null, arguments);
                }
                else {
                    contractFunction = contract.methods[methodname]();
                }
                contractFunction.call({ from: pubkey }, function (err, res) {
                    if (!err) {
                        console.log(res);
                    }
                    else {
                        console.error("ERROR", err);
                    }
                })
            }
            else {
                console.log("method type is undefined");
            }

        }


//token registry-----------
// token_owner_address,token_symbol,swarm_hash

// let args =[pubkey,"CATE","catetoken","0xec37f2e96b461e635284ce8df373ae613a34a485faee918748d4602656be36b2"];
// transactionfunction("payablefunction","TokenRegistry","registerTicker",args,"0x00");
// 0x3aa5a471e05642a6e4c7e15787d02b6d03f09bf7165f977ad44ba2088e3ca9fe 

// Getting token details
// token symbol

// let args=["CATE"];
// transactionfunction("constantfunction","TokenRegistry","getDetails",args,"0x00")

//token deploy-------------
// name,symbol,decimal,tokendetails

// let args =["catetoken","CATE",18,"0x657468657265756d000000000000000000000000000000000000000000000000"];
// transactionfunction("payablefunction","SecurityTokenRegistry","generateSecurityToken",args,"0x00");
// transhash = 0x3f0633b91bcc6367db8aa7a381d990953c07f0f0005f14191bbb620a9ef99eb3

// get token address
//token symbol

// let args=["CATE"];
// transactionfunction("constantfunction","SecurityTokenRegistry","getSecurityTokenAddress",args,"0x00");
// SecurityToken contrat address == 0x75A68967f449BCe0F70A589F95f9082ce295d6Ae

//getModule -  GeneralTransferManager through security token-----------------
//2 , 0

// let args=[2,0]
// transactionfunction("constantfunction","SecurityToken","getModule",args,"0x00");
// GeneralTransferManager contract address == 0x09A4f4deCcD275892197F3BeFD808Ea36480cEA2


// Adding whitelist to Security token
// frm = 1555032633  to = 1555119033  invester address = 0x1a19358B41F994BBe37Be0d409F2238f936fF5D3
// investor,from,to 

// let args =["0x1a19358B41F994BBe37Be0d409F2238f936fF5D3",1555032633,1555119033,];
// transactionfunction("payablefunction","GeneralTransferManager","modifyWhitelist",args,"0x00");
// transhash = 0x266fc5272f3251c4ab424b52c62b00a1bcf1dbe214d013f2b63fc077c34a7db4

//mint token to whitelisted investors
//token can be minted before launching the token
//investor,amount to be mint (ex: 100000000000000000000000 for 100,000)

// let args =["0x1a19358B41F994BBe37Be0d409F2238f936fF5D3","100000000000000000000000"];
// transactionfunction("payablefunction","SecurityToken","mint",args,"0x00");
// transhash = 0x10b24aa41df86fbe72154333b2f5df8c74471d22d2adcab903d18ed619f1b13b


//Adding CappedSto module to Security token contract
//0x423105526255dB03C36FD763C982daD96B95D17e
// 0xdd2761de000000000000000000000000000000000000000000000000000000005caff141000000000000000000000000000000000000000000000000000000005cb142c100000000000000000000000000000000000000000000d3c21bcecceda100000000000000000000000000000000000000000000000000000000000000000003e80000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000075a68967f449bce0f70a589f95f9082ce295d6ae
// factory address,data,max_cost,budget,locked

// let args = ["0x423105526255dB03C36FD763C982daD96B95D17e", "0xdd2761de000000000000000000000000000000000000000000000000000000005caff141000000000000000000000000000000000000000000000000000000005cb142c100000000000000000000000000000000000000000000d3c21bcecceda100000000000000000000000000000000000000000000000000000000000000000003e80000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000075a68967f449bce0f70a589f95f9082ce295d6ae", 0, 0, true];
// transactionfunction("payablefunction", "SecurityToken", "addModule", args, "0x00");
//  transhash-fail = 0x2df524f41f060f3133dfa138498cf0e6cd610cd21f7768b92082c8bf430ddb15